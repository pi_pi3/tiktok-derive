use std::rc::Rc;

use proc_macro2::{TokenStream, Ident, Span};
use quote::{quote, ToTokens, TokenStreamExt};

#[derive(Debug, Clone, PartialEq)]
pub struct RegExpr {
    pub sum: Sum,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Sum {
    pub sum: Vec<Prod>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Prod {
    pub prod: Vec<Item>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Item {
    Star(Atom),
    Plus(Atom),
    Maybe(Atom),
    Quiet(Atom),
    Between(Atom, Option<u64>, Option<u64>),
    Atom(Atom),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Atom {
    Dot,
    Char(char),
    Str(Rc<String>),
    Range(char, char),
    NotRange(char, char),
    Any(Rc<String>),
    NotAny(Rc<String>),
    Named(Rc<String>),
    Group(Rc<RegExpr>),
}

impl ToTokens for RegExpr {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let sum = &self.sum;
        tokens.append_all(quote! { #sum });
    }
}

impl ToTokens for Sum {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let first = &self.sum[0];
        let mut quote = quote! { #first };
        for prod in self.sum.iter().skip(1) {
            quote = quote! { #quote.or(#prod) };
        }
        tokens.append_all(quote);
    }
}

impl ToTokens for Prod {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let tok = match self.prod.first() {
            Some(first) => {
                let mut quote = quote! { #first };
                for item in self.prod.iter().skip(1) {
                    quote = quote! { #quote.and(#item) };
                }
                quote
            }
            None => quote! { ::tiktok::pattern::Count::new(0) },
        };
        tokens.append_all(tok);
    }
}

impl ToTokens for Item {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let tok = match self {
            Item::Star(atom) => quote! { ::tiktok::pattern::ZeroPlus::new(#atom) },
            Item::Plus(atom) => quote! { ::tiktok::pattern::OnePlus::new(#atom) },
            Item::Maybe(atom) => quote! { ::tiktok::pattern::Maybe::new(#atom) },
            Item::Quiet(atom) => quote! { ::tiktok::pattern::Quiet::new(#atom) },
            Item::Between(atom, a, b) => quote! { ::tiktok::pattern::Between::new(#atom, #a, #b) },
            Item::Atom(atom) => quote! { #atom },
        };
        tokens.append_all(tok);
    }
}

impl ToTokens for Atom {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let tok = match self {
            Atom::Dot => quote! { ::tiktok::pattern::Count::new(1) },
            Atom::Char(c) => quote! { ::tiktok::pattern::Char::new(#c) },
            Atom::Str(s) => {
                let s = &**s;
                quote! { ::tiktok::pattern::Str::new(#s) }
            }
            Atom::Range(a, b) => quote! { ::tiktok::pattern::Range::new(#a, #b) },
            Atom::NotRange(a, b) => quote! { ::tiktok::pattern::NotRange::new(#a, #b) },
            Atom::Any(s) => {
                let s = &**s;
                quote! { ::tiktok::pattern::Any::new(#s) }
            }
            Atom::NotAny(s) => {
                let s = &**s;
                quote! { ::tiktok::pattern::NotAny::new(#s) }
            }
            Atom::Named(s) => {
                let ident = Ident::new(s, Span::call_site());
                quote! { #ident::default() }
            }
            Atom::Group(expr) => {
                let expr = &**expr;
                quote! { ::tiktok::pattern::Capture::new(#expr) }
            }
        };
        tokens.append_all(tok);
    }
}
