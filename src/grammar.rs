use std::rc::Rc;

use combs::combinator::*;

use crate::ast::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Never {}

macro_rules! parser {
    { $( pub struct $name:ident |$tok:ty| $out:ty => $body:block; )* } => {
        $(
            pub struct $name;

            impl ::combs::combinator::Combinator for $name {
                type Error = Never;
                type Token = $tok;
                type Output = $out;

                fn try_match<I>(&self, iter: &mut ::combs::BufferedIterator<I>)
                    -> Option<Result<::combs::combinator::Match<Self::Token, Self::Output>, Self::Error>>
                where
                    I: Iterator<Item = Result<Self::Token, Self::Error>>
                {
                    $body.try_match(iter)
                }
            }
        )*
    }
}

parser! {
    pub struct AnyChar |char| char => {
        Count::new(1)
            .map(|m| m.token[0])
    };

    pub struct Schar |char| char => {
        Literal::new('\\')
            .and(Literal::new('"')
                .or(Literal::new('\\'))
                .or(Literal::new('['))
                .or(Literal::new('-'))
                .or(Literal::new(']'))
                .or(Literal::new('\''))
                .or(Literal::new('$'))
                .or(Literal::new(':')))
            .map(|m| m.token[1])
            .or(Predicate::new(|&c0| {
                for c1 in "[-]\"'$:".chars() {
                    if c0 == c1 {
                        return false;
                    }
                }
                true
            })
                .map(|m| m.token[0]))
    };

    pub struct Ichar |char| char => {
        Predicate::new(|&c0| {
            c0 >= 'a' && c0 <= 'z'
                || c0 >= 'A' && c0 <= 'Z'
                || c0 >= '0' && c0 <= '9'
                || c0 == '_'
        })
            .map(|m| m.token[0])
    };

    pub struct Dchar |char| char => {
        Predicate::new(|&c0| {
            c0 >= '0' && c0 <= '9'
        })
            .map(|m| m.token[0])
    };

    pub struct StrLit |char| Rc<String> => {
        Schar.zero_plus()
            .map(|m| {
                let string = m.value.iter().fold(String::new(), |mut acc, &c| {
                    acc.push(c);
                    acc
                });
                Rc::new(string)
            })
    };

    pub struct IdentLit |char| Rc<String> => {
        Ichar.zero_plus()
            .map(|m| {
                let string = m.value.iter().fold(String::new(), |mut acc, &c| {
                    acc.push(c);
                    acc
                });
                Rc::new(string)
            })
    };

    pub struct NumberLit |char| String => {
        Dchar.zero_plus()
            .map(|m| m.value.iter().fold(String::new(), |mut acc, &c| {
                acc.push(c);
                acc
            }))
    };

    pub struct Char |char| Atom => {
        Literal::new('\'')
            .and_with(AnyChar)
            .map(|m| Atom::Char(m.value))
    };

    pub struct Str |char| Atom => {
        Literal::new('"')
            .and_with(StrLit)
            .and_without(Literal::new('"'))
            .map(|m| Atom::Str(Rc::clone(&m.value)))
    };

    pub struct Dot |char| Atom => {
        Literal::new('.')
            .map(|_| Atom::Dot)
    };

    pub struct Range |char| Atom => {
        Literal::new('[')
            .and_with(Schar)
            .and_without(Literal::new('-'))
            .and(Schar)
            .and_without(Literal::new(']'))
            .map(|m| Atom::Range(m.value.0, m.value.1))
    };

    pub struct NotRange |char| Atom => {
        Literal::new('[')
            .and_without(Literal::new('^'))
            .and_with(Schar)
            .and_without(Literal::new('-'))
            .and(Schar)
            .and_without(Literal::new(']'))
            .map(|m| Atom::NotRange(m.value.0, m.value.1))
    };

    pub struct Any |char| Atom => {
        Literal::new('[')
            .and_with(StrLit)
            .and_without(Literal::new(']'))
            .map(|m| Atom::Any(Rc::clone(&m.value)))
    };

    pub struct NotAny |char| Atom => {
        Literal::new('[')
            .and_without(Literal::new('^'))
            .and_with(StrLit)
            .and_without(Literal::new(']'))
            .map(|m| Atom::NotAny(Rc::clone(&m.value)))
    };

    pub struct Named |char| Atom => {
        Literal::new('$')
            .and_with(IdentLit)
            .map(|m| Atom::Named(Rc::clone(&m.value)))
    };

    pub struct Group |char| Atom => {
        Literal::new('(')
            .and_with(RegExprParser)
            .and_without(Literal::new(')'))
            .map(|m| Atom::Group(Rc::new(m.value.clone())))
    };

    pub struct AtomParser |char| Atom => {
        Group
            .or(Named)
            .or(NotRange)
            .or(Range)
            .or(NotAny)
            .or(Any)
            .or(Dot)
            .or(Str)
            .or(Char)
    };

    pub struct AtomItem |char| Item => {
        AtomParser.map(|m| Item::Atom(m.value.clone()))
    };

    pub struct Star |char| Item => {
        AtomParser
            .and_without(Literal::new('*'))
            .map(|m| Item::Star(m.value.clone()))
    };

    pub struct Plus |char| Item => {
        AtomParser
            .and_without(Literal::new('+'))
            .map(|m| Item::Plus(m.value.clone()))
    };

    pub struct Maybe |char| Item => {
        AtomParser
            .and_without(Literal::new('?'))
            .map(|m| Item::Maybe(m.value.clone()))
    };

    pub struct Quiet |char| Item => {
        AtomParser
            .and_without(Literal::new('%'))
            .map(|m| Item::Quiet(m.value.clone()))
    };

    pub struct Between |char| Item => {
        AtomParser
            .and_without(Literal::new('{'))
            .and(NumberLit.maybe()
                .and_without(Literal::new(','))
                .and(NumberLit.maybe()))
            .and_without(Literal::new('}'))
            .map(|m| {
                Item::Between(
                    m.value.0.clone(),
                    (m.value.1).0.as_ref().map(|s| s.parse().expect("something went wrong while parsing decimals")),
                    (m.value.1).1.as_ref().map(|s| s.parse().expect("something went wrong while parsing decimals")),
                )
            })
    };

    pub struct ItemParser |char| Item => {
        Between
            .or(Quiet)
            .or(Maybe)
            .or(Plus)
            .or(Star)
            .or(AtomItem)
    };

    pub struct ProdParser |char| Prod => {
        Predicate::new(|c: &char| c.is_whitespace())
            .zero_plus()
            .and_with(ItemParser
                .and_without(Predicate::new(|c: &char| c.is_whitespace()).zero_plus()))
            .zero_plus()
            .map(|m| Prod { prod: m.value.clone() })
    };

    pub struct SumParser |char| Sum => {
        ProdParser
            .and(Literal::new('|')
                 .and_with(ProdParser)
                 .zero_plus())
            .map(|m| {
                let mut sum = vec![m.value.0.clone()];
                sum.extend(m.value.1.iter().map(Clone::clone));
                Sum { sum }
            })
    };

    pub struct RegExprParser |char| RegExpr => {
        SumParser.map(|m| RegExpr { sum: m.value.clone() })
    };
}
