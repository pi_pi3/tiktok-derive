#![recursion_limit = "128"]
#![warn(rust_2018_idioms)]

extern crate proc_macro;

mod ast;
mod grammar;

use quote::quote;
use syn::{parse_macro_input, DeriveInput, Lit, Meta, MetaNameValue, NestedMeta, Data};

use combs::{combinator::Combinator, BufferedIterator};

use self::grammar::RegExprParser;

#[proc_macro_derive(Pattern, attributes(pattern))]
pub fn derive_pattern(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Parse the input tokens into a syntax tree.
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let mut pattern = None;
    for attr in input.attrs {
        if attr.path.is_ident("pattern") {
            match attr.parse_meta() {
                Ok(Meta::NameValue(MetaNameValue {
                    lit: Lit::Str(lit_str), ..
                })) => {
                    pattern = Some(lit_str);
                }
                _ => {
                    panic!("expected `#[pattern = \"...\"]`");
                }
            }
        }
    }

    if pattern.is_none() {
        panic!("expected `#[pattern = \"...\"]`");
    }

    let pattern = pattern.unwrap().value();
    let mut buf = BufferedIterator::new(pattern.chars().map(Ok));
    let m = RegExprParser.try_match(&mut buf).unwrap_or_else(|| {
        panic!("invalid pattern: `{}`", pattern);
    });
    assert!(buf.next().is_none(), "invalid pattern near eof: `{}`", pattern);
    let inner_value = m.unwrap().value;

    let expanded = quote! {
        impl ::tiktok::pattern::Pattern for #name {
            fn try_match(&self, span: &::tiktok::span::Span)
                -> Option<::tiktok::pattern::Match>
            {
                #inner_value.try_match(span)
            }
        }
    };

    // Hand the output tokens back to the compiler.
    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(Token, attributes(kind))]
pub fn derive_token(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Parse the input tokens into a syntax tree.
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let mut kind = None;
    for attr in input.attrs {
        if attr.path.is_ident("kind") {
            match attr.parse_meta() {
                Ok(Meta::List(list)) => {
                    for meta in list.nested {
                        match meta {
                            NestedMeta::Meta(Meta::Word(ident)) => kind = Some(ident),
                            _ => {
                                panic!("expected `#[kind(...)]`");
                            }
                        }
                    }
                }
                _ => {
                    panic!("expected `#[kind(...)]`");
                }
            }
        }
    }

    if kind.is_none() {
        panic!("expected `#[kind(...)]`");
    }

    let kind = kind.unwrap();

    let expanded = quote! {
        impl ::tiktok::tokenizer::Token for #name {
            type TokenKind = #kind;

            fn try_match<'a>(span: &::tiktok::span::Span) -> Option<(::tiktok::span::Span, Self)> {
                use ::tiktok::tokenizer::TokenKind;
                #kind::try_match(span)
                    .map(|(span, remaining, token)| {
                        let token = Token {
                            span,
                            token,
                        };
                        (remaining, token)
                    })
            }
        }
    };

    // Hand the output tokens back to the compiler.
    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(TokenKind, attributes(pattern))]
pub fn derive_token_kind(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Parse the input tokens into a syntax tree.
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let mut variants = vec![];
    match input.data {
        Data::Enum(data) => {
            for variant in data.variants {
                let mut pattern = None;
                for attr in variant.attrs {
                    if attr.path.is_ident("pattern") {
                        match attr.parse_meta() {
                            Ok(Meta::List(list)) => {
                                for meta in list.nested {
                                    match meta {
                                        NestedMeta::Meta(Meta::Word(ident)) => pattern = Some(ident),
                                        _ => {
                                            panic!("expected `#[pattern(...)]` on each variant");
                                        }
                                    }
                                }
                            }
                            _ => {
                                panic!("expected `#[pattern(...)]` on each variant");
                            }
                        }
                    }
                }

                if pattern.is_none() {
                    panic!("expected `#[pattern(...)]` on each variant");
                }

                let pattern = pattern.unwrap();

                variants.push((variant.ident, pattern));
            }
        }
        _ => panic!("expected an enum"),
    }

    let mut if_chain = quote! {};
    for (ident, pattern) in variants {
        if_chain = quote! {
            #if_chain
            if let Some(m) = #pattern.try_match(span) {
                let remaining = span.respan(m.end()..span.end()).unwrap();
                let token = #name::#ident;
                Some((m.span().clone(), remaining, token))
            } else
        };
    }

    let expanded = quote! {
        impl ::tiktok::tokenizer::TokenKind for #name {
            fn try_match<'a>(span: &::tiktok::span::Span)
                -> Option<(::tiktok::span::Span, ::tiktok::span::Span, Self)> {
                #if_chain
                {
                    None
                }
            }
        }
    };

    // Hand the output tokens back to the compiler.
    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(TokenIter)]
pub fn derive_token_iter(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Parse the input tokens into a syntax tree.
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let expanded = quote! {
        impl Iterator for #name {
            type Item = Result<
                <Self as ::tiktok::tokenizer::Tokenizer>::Token,
                <Self as ::tiktok::tokenizer::Tokenizer>::Error,
            >;
            fn next(&mut self) -> Option<Self::Item> {
                ::tiktok::tokenizer::Tokenizer::next(self)
            }
        }
    };

    // Hand the output tokens back to the compiler.
    proc_macro::TokenStream::from(expanded)
}
