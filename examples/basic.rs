#[macro_use]
extern crate tiktok_derive;

use tiktok::span::Span;
use tiktok::pattern::Pattern;

#[derive(Pattern)]
#[pattern = "\"foobar\""]
pub struct Pat;

#[derive(Pattern)]
#[pattern = "\"foo\".\"bar\""]
pub struct DotPat;

#[derive(Pattern)]
#[pattern = "\"foo\"|\"bar\""]
pub struct OrPat;

#[derive(Default, Pattern)]
#[pattern = r#" "foo" | "bar" "baz" "#]
pub struct SpacedPat;

#[derive(Pattern)]
#[pattern = r#" $SpacedPat "#]
pub struct NamedPat;

#[derive(Default, Pattern)]
#[pattern = r#" "a" $RecursivePat | "b" "#]
pub struct RecursivePat;

#[derive(Default, Pattern)]
#[pattern = r#" '. "#]
pub struct CharPat;

#[derive(Default, Pattern)]
#[pattern = r#" "\\" "#]
pub struct BackslashPat;

pub fn main() {
    assert_eq!(Pat.try_match(&Span::with_str(None, "foobar")).unwrap(), "foobar");
    assert_eq!(Pat.try_match(&Span::with_str(None, "foo")), None);
    assert_eq!(DotPat.try_match(&Span::with_str(None, "foo_bar")).unwrap(), "foo_bar");
    assert_eq!(DotPat.try_match(&Span::with_str(None, "foo_baz")), None);
    assert_eq!(OrPat.try_match(&Span::with_str(None, "foo")).unwrap(), "foo");
    assert_eq!(OrPat.try_match(&Span::with_str(None, "bar")).unwrap(), "bar");
    assert_eq!(OrPat.try_match(&Span::with_str(None, "baz")), None);
    assert_eq!(SpacedPat.try_match(&Span::with_str(None, "foo")).unwrap(), "foo");
    assert_eq!(SpacedPat.try_match(&Span::with_str(None, "barbaz")).unwrap(), "barbaz");
    assert_eq!(SpacedPat.try_match(&Span::with_str(None, "qux")), None);
    assert_eq!(RecursivePat.try_match(&Span::with_str(None, "b")).unwrap(), "b");
    assert_eq!(RecursivePat.try_match(&Span::with_str(None, "ab")).unwrap(), "ab");
    assert_eq!(RecursivePat.try_match(&Span::with_str(None, "aab")).unwrap(), "aab");
    assert_eq!(RecursivePat.try_match(&Span::with_str(None, "a")), None);
    assert_eq!(CharPat.try_match(&Span::with_str(None, ".foo")).unwrap(), ".");
    assert_eq!(CharPat.try_match(&Span::with_str(None, "foo")), None);
    assert_eq!(BackslashPat.try_match(&Span::with_str(None, "\\")).unwrap(), "\\");
    assert_eq!(BackslashPat.try_match(&Span::with_str(None, "foo")), None);
}
