#[macro_use]
extern crate tiktok_derive;

use tiktok::span::Span;
use tiktok::tokenizer::{self, Token as TikToken};
use tiktok::pattern::Pattern;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Unexpected(pub usize);

#[derive(Debug, Pattern)]
#[pattern = "\"foo\""]
pub struct Foo;

#[derive(Debug, Pattern)]
#[pattern = "\"bar\""]
pub struct Bar;

#[derive(Debug, Clone, PartialEq, Eq, Token)]
#[kind(TokenKind)]
pub struct Token {
    span: Span,
    token: TokenKind,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, TokenKind)]
pub enum TokenKind {
    #[pattern(Foo)]
    Foo,
    #[pattern(Bar)]
    Bar,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Tokenizer {
    pub input: Span,
}

impl tokenizer::Tokenizer for Tokenizer {
    type Error = Unexpected;
    type Token = Token;

    fn next(&mut self) -> Option<Result<Self::Token, Self::Error>> {
        if self.input.is_empty() {
            return None;
        }

        match Token::try_match(&self.input) {
            Some((remaining, token)) => {
                self.input = remaining;
                Some(Ok(token))
            }
            None => Some(Err(Unexpected(self.input.start()))),
        }
    }
}

impl Iterator for Tokenizer {
    type Item = Result<<Self as tokenizer::Tokenizer>::Token, <Self as tokenizer::Tokenizer>::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        tokenizer::Tokenizer::next(self)
    }
}

pub fn main() {
    let tok = Tokenizer {
        input: Span::with_str(None, "foobarbarfoofoofoobarxxxbar"),
    };
    for token in tok {
        match token {
            Ok(token) => println!("{:?}", token),
            Err(err) => {
                eprintln!("error: {:?}", err);
                break;
            }
        }
    }
}
