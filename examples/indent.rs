#[macro_use]
extern crate tiktok_derive;

use tiktok::span::Span;
use tiktok::pattern::{self, Pattern, IndentEq, IndentDown, IndentUp};

static mut INDENT: Option<(IndentEq, IndentDown, IndentUp)> = None;

fn indent() -> (IndentEq, IndentDown, IndentUp) {
    unsafe {
        if INDENT.is_none() {
            INDENT = Some(pattern::indent(None));
        }

        INDENT.as_ref().unwrap().clone()
    }
}

pub struct IEq;

impl IEq {
    pub fn default() -> IndentEq {
        indent().0
    }
}

pub struct IDown;

impl IDown {
    pub fn default() -> IndentDown {
        indent().1
    }
}

pub struct IUp;

impl IUp {
    pub fn default() -> IndentUp {
        indent().2
    }
}

#[derive(Default, Pattern)]
#[pattern = r#" "foo" $IUp "bar" $IEq "baz" $IDown "#]
pub struct IndentPat;

pub fn main() {
    assert_eq!(
        IndentPat
            .try_match(&Span::with_str(None, "foo\n  bar\n  baz\n"))
            .unwrap(),
        "foo\n  bar\n  baz\n"
    );
    assert_eq!(IndentPat.try_match(&Span::with_str(None, "foo\n  bar\n baz\n")), None);
    assert_eq!(IndentPat.try_match(&Span::with_str(None, "foo\nbar\nbaz\n")), None);
    assert_eq!(IndentPat.try_match(&Span::with_str(None, "foo\n  bar\n  baz")), None);
    assert_eq!(IndentPat.try_match(&Span::with_str(None, "foo bar baz")), None);
}
